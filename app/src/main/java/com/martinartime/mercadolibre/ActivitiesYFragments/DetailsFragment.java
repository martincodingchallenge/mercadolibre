package com.martinartime.mercadolibre.ActivitiesYFragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.martinartime.mercadolibre.POJO.APIItemDetails;
import com.martinartime.mercadolibre.POJO.Picture;
import com.martinartime.mercadolibre.POJO.Result;
import com.martinartime.mercadolibre.R;
import com.synnapps.carouselview.CarouselView;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.martinartime.mercadolibre.App.apiService;

/**
 * Creado por MartinArtime el 28 de noviembre del 2019
 */
public class DetailsFragment extends Fragment {

    @BindView(R.id.carouselView) CarouselView carouselView;
    @BindView(R.id.txt_amount_sold) TextView txtAmountSold;
    @BindView(R.id.txt_product_name) TextView txtProductName;
    @BindView(R.id.txt_product_price) TextView txtProductPrice;
    @BindView(R.id.txt_stock_available) TextView txtHasStockAvailable;
    @BindView(R.id.txt_product_condition) TextView txtProductCondition;
    @BindView(R.id.txt_seller_geo) TextView txtSellerGeo;
    @BindView(R.id.txt_free_shipping) TextView txtIsFreeShipping;

    private List<Picture> pictures;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, viewGroup, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity parentActivity = (MainActivity) getActivity();

        // If main activity is not null I can fill all fields of the result selected
        if(parentActivity!=null){
            Result result = parentActivity.resultSelected;

            // I make an async search to fetch the good quality pictures of the items
            parentActivity.compositeDisposable.add(apiService.searchItem(result.getId())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));

            // Set all fields

            // Amount sold
            Integer sold = result.getSoldQuantity();
            if(sold==0){
                txtAmountSold.setVisibility(View.INVISIBLE);
            } else {
                txtAmountSold.setText(result.getSoldQuantity()+" vendidos");
            }

            // Item name
            txtProductName.setText(result.getTitle());

            // Item price
            NumberFormat formatter = NumberFormat.getInstance(new Locale("da", "DK"));
            String priceToShow = formatter.format(result.getPrice().intValue());
            txtProductPrice.setText("$ "+priceToShow);

            // Available quantity
            Integer quantity = result.getAvailableQuantity();
            if(quantity==0){
                txtHasStockAvailable.setText("No queda stock de este producto");
            } else if(quantity==1){
                txtHasStockAvailable.setText("Último en stock!");
            } else {
                txtHasStockAvailable.setText("Quedan "+quantity+" en stock!");
            }

            // Item sale condition
            String condition = result.getCondition();
            if(condition.equals("new")){
                txtProductCondition.setText("El producto es nuevo!");
            } else if(condition.equals("used")){
                txtProductCondition.setText("El producto es usado!");
            } else {
                txtProductCondition.setVisibility(View.INVISIBLE);
            }

            // Seller province/city name
            txtSellerGeo.setText("El vendedor es de "+result.getAddress().getCityName());

            // If has free shipping, let the customer know!
            if(result.getShipping().getFreeShipping()){
                txtIsFreeShipping.setText("Envío gratis!");
            }
        }
    }

    /**
      * Handle response of API call to fetch good quality item pictures
     */
    private void handleResponse(APIItemDetails details) {
        pictures = details.getPictures();

        carouselView.setImageListener((position, imageView) ->
                        Glide.with(this)
                                .load(pictures.get(position).getUrl())
                                .fitCenter()
                                .into(imageView));
        carouselView.setPageCount(pictures.size());
    }

    /**
     * Handle error of api call to fetch pictures of item
     */
    private void handleError(Throwable error) {
        Log.e("TAG", "handleError: "+ error.getLocalizedMessage());
        //Toast.makeText(getActivity(), "Error! No se pudo buscar correctamente. "+error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

}
