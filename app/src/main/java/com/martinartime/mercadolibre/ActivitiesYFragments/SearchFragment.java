package com.martinartime.mercadolibre.ActivitiesYFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martinartime.mercadolibre.App;
import com.martinartime.mercadolibre.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creado por MartinArtime el 28 de noviembre del 2019
 */
public class SearchFragment extends Fragment {

    @BindView(R.id.recycler_view) RecyclerView frame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, viewGroup, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity activity = (MainActivity) getActivity();
        if(activity!=null){
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(App.getContext());
            frame.setLayoutManager(mLayoutManager);
            frame.setItemAnimator(new DefaultItemAnimator());
            frame.setAdapter(activity.resultAdapter);

            activity.resultAdapter.notifyDataSetChanged();
        }

    }

}
