package com.martinartime.mercadolibre.ActivitiesYFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.martinartime.mercadolibre.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creado por MartinArtime el 28 de noviembre del 2019
 */
public class ShimmerFragment extends Fragment {

    @BindView(R.id.shimmer_view_container) ShimmerFrameLayout frame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shimmer, viewGroup, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // When shimmer is called I start the animation
        startAnimation();
    }

    // Class method to start the animation
    private void startAnimation(){
        frame.setVisibility(View.VISIBLE);
        frame.startShimmerAnimation();
    }

    // Class method to stop the animation and hide the view
    void stopAnimation(){
        frame.stopShimmerAnimation();
        frame.setVisibility(View.GONE);
    }

}
