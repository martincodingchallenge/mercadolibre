package com.martinartime.mercadolibre.ActivitiesYFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.martinartime.mercadolibre.Adapters.HistoryAdapter;
import com.martinartime.mercadolibre.App;
import com.martinartime.mercadolibre.R;
import com.martinartime.mercadolibre.Utils.LocalStorageManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.martinartime.mercadolibre.Utils.Util.SEARCH_HISTORY;

/**
 * Creado por MartinArtime el 28 de noviembre del 2019
 */
public class HistoryFragment extends Fragment {

    @BindView(R.id.recycler_view_history) RecyclerView frame;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, viewGroup, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        MainActivity activity = (MainActivity) getActivity();
        if(activity!=null){

            // I save in shared preferences the history of searches
            ArrayList<String> historial = LocalStorageManager.obtenerHistorial(SEARCH_HISTORY);
            HistoryAdapter historyAdapter = new HistoryAdapter(historial, activity);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(App.getContext());
            frame.setLayoutManager(mLayoutManager);
            frame.setItemAnimator(new DefaultItemAnimator());
            frame.setAdapter(historyAdapter);

            historyAdapter.notifyDataSetChanged();
        }

    }

}
