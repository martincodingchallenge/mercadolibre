package com.martinartime.mercadolibre.ActivitiesYFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.martinartime.mercadolibre.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Creado por MartinArtime el 28 de noviembre del 2019
 */
public class DefaultFragment extends Fragment {

    private MainActivity parentActivity;

    @BindView(R.id.im_default) ImageView image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_default, viewGroup, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        parentActivity = (MainActivity) getActivity();

        // I listen to the click of the search image on the main view
        image.setOnClickListener(v -> parentActivity.clickSearchBar(getView()));

    }

}
