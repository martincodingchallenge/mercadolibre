package com.martinartime.mercadolibre.ActivitiesYFragments;

import android.app.SearchManager;
import android.content.Context;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.martinartime.mercadolibre.API.APIService;
import com.martinartime.mercadolibre.Adapters.HistoryAdapter;
import com.martinartime.mercadolibre.Adapters.ResultAdapter;
import com.martinartime.mercadolibre.POJO.APISearchResponse;
import com.martinartime.mercadolibre.POJO.Result;
import com.martinartime.mercadolibre.R;
import com.martinartime.mercadolibre.Utils.LocalStorageManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import static com.martinartime.mercadolibre.App.getAPIService;
import static com.martinartime.mercadolibre.App.getInstance;
import static com.martinartime.mercadolibre.Utils.Util.SEARCH_HISTORY;

/**
 *  Creado por MartinArtime el 27 de noviembre del 2019
 *
 *  Main Activity in charge of displaying and managing all fragments of the app
 */
public class MainActivity extends AppCompatActivity implements ResultAdapter.ResultAdapterListener,
        HistoryAdapter.HistoryAdapterListener {

    public List<Result> resultList = new ArrayList<>();
    public ResultAdapter resultAdapter;
    public CompositeDisposable compositeDisposable;

    private SearchView searchView;
    private APIService apiService;

    SearchFragment fgSearch;
    DefaultFragment fgDefault;
    ShimmerFragment fgShimmer;
    DetailsFragment fgDetails;
    HistoryFragment fgHistory;

    Menu menu;
    private String search;
    public Result resultSelected;

    @BindView(R.id.toolbar) Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        // I create the toolbar and set it to the main view
        initToolbar();

        // Objects initialization
        compositeDisposable = new CompositeDisposable();
        apiService = getAPIService();

        resultAdapter = new ResultAdapter(this, resultList, this);

        fgSearch = new SearchFragment();
        fgDefault = new DefaultFragment();
        fgShimmer = new ShimmerFragment();
        fgDetails = new DetailsFragment();
        fgHistory = new HistoryFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Set main view as the default fragment
        changeFragment(fgDefault);
    }


    /**
     * Method that changes the fragment replacing the one currently displaying
     * @param fragment that will be now displayed
     */
    private void changeFragment(Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment);
        transaction.commit();
    }

    /**
     * Customize the toolbar and display it
     */
    private void initToolbar(){
        toolbar.setTitle(R.string.toolbar_title);
        toolbar.setTitleTextColor(ColorStateList.valueOf(getResources().getColor(R.color.colorNegro, getInstance().getTheme())));
        toolbar.setNavigationOnClickListener(v -> this.onBackPressed());
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    /**
     * Make API call with the query set by user in toolbar's search view
     * @param query: string to make api call with user search
     */
    private void fetchResults(String query) {

        // Add to a composite disposable observable
        compositeDisposable.add(apiService.searchProducts(query)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::handleResponse,this::handleError));
    }

    /**
     * If the api call is successful it handles the repsonse
     * @param response: search query result to parse
     */
    private void handleResponse(APISearchResponse response) {
        // Add all results to list
        resultList.clear();
        resultList.addAll(response.getResults());

        // Stop "dummy" animation
        fgShimmer.stopAnimation();

        // Change fragment to search
        changeFragment(fgSearch);

        // Wait one second to refresh fragment and display results
        resultAdapter.notifyDataSetChanged();
        Handler handler = new Handler();
        handler.postDelayed(() -> resultAdapter.getFilter().filter(search), 1000);
    }

    /**
     * If api call was not successful, we manage the error
     * @param error: what happened?!?!
     */
    private void handleError(Throwable error) {
        Toast.makeText(this, "Error! No se pudo buscar correctamente.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // We clear the composite disposable to avoid a memory leak
        compositeDisposable.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                search = query;

                // It saves in shared preferences the history of searches, adding the new one
                LocalStorageManager.guardarHistorial(SEARCH_HISTORY, search);

                // Close input keyboard
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                // Change to shimmer 'dummy search' fragment
                changeFragment(fgShimmer);

                // Search results in background async
                fetchResults(query);

                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                // Filter user searches
                resultAdapter.getFilter().filter(query);
                return true;
            }
        });
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {

            // If searching something change to display history of searches
            changeFragment(fgHistory);

        } else if(id == android.R.id.home) {

            // If pressing back button, display default view
            changeFragment(fgDefault);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onResultSelected(Result result) {

        // If user clicked on an item in the search/results fragment we go to the details fragment
        resultSelected = result;
        changeFragment(fgDetails);
    }

    @Override
    public void onHistorySelected(String resultado) {

        // If user clicked on an item in the history fragment we go to the details fragment with that search
        search = resultado;
        searchView.setQuery(search, true);
    }

    // To manually perform click on search bar (if user clicked on image view on default fragment)
    public void clickSearchBar(View view) {
        menu.performIdentifierAction(R.id.action_search, 0);
    }
}
