package com.martinartime.mercadolibre.Utils;

/**
 * Creado por MartinArtime el 26 de noviembre del 2019
 */
public class Util {

    public static final String URL_BASE = "https://api.mercadolibre.com/";
    public static final String URL_SEARCH = "sites/MLA/search/";
    public static final String URL_ITEMS = "items/";
    public static final String SHARED_PREFERENCES = "com.martinartime.mercadolibre";
    public static final String SEARCH_HISTORY = "com.martinartime.mercadolibre.searchhistory";

}
