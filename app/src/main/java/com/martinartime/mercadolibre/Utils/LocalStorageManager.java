package com.martinartime.mercadolibre.Utils;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.martinartime.mercadolibre.App;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.content.Context.MODE_PRIVATE;
import static com.martinartime.mercadolibre.App.gson;
import static com.martinartime.mercadolibre.Utils.Util.SHARED_PREFERENCES;

/**
 * Helper class designed to make saving and retrieving from shared preferences, easier
 */
public class LocalStorageManager {

    /**
     * Static method that save the search history in shared preferences (adding the new element)
     */
    public static void guardarHistorial(String key, String element){

        ArrayList<String> historyList;

        SharedPreferences preferences = App.getInstance().getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = preferences.edit();

        String history = preferences.getString(key, "");
        if(history!=null && !history.equals("")){
            historyList = gson.fromJson(history, new TypeToken<ArrayList<String>>() {}.getType());
        } else {
            historyList = new ArrayList<>();
        }

        if(!historyList.contains(element)){
            historyList.add(element);
            prefsEditor.putString(key, gson.toJson(historyList));
            prefsEditor.apply();
        }
    }

    /**
     * Static method that retrieves the search history from shared preferences
     */
    public static ArrayList<String> obtenerHistorial(String key) {
        SharedPreferences preferences = App.getInstance().getSharedPreferences(SHARED_PREFERENCES, MODE_PRIVATE);
        String history = preferences.getString(key, "");
        if(history!=null && !history.equals("")) {
            return gson.fromJson(history, new TypeToken<List<String>>() {}.getType());
        } else {
            return null;
        }
    }

}
