package com.martinartime.mercadolibre.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Creado por MartinArtime el 28 de noviembre del 2019
 */
public class APIItemDetails {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("site_id")
    @Expose
    private String siteId;

    @SerializedName("pictures")
    @Expose
    private List<Picture> pictures;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId(String siteId) {
        this.siteId = siteId;
    }

    public List<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(List<Picture> pictures) {
        this.pictures = pictures;
    }
}
