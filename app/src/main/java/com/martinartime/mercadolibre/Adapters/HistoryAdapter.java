package com.martinartime.mercadolibre.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.martinartime.mercadolibre.R;

import java.util.ArrayList;

/**
 * Creado por MartinArtime el 27 de noviembre del 2019
 */
public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.MyViewHolder> {

    private ArrayList<String> resultsList;
    private HistoryAdapter.HistoryAdapterListener listener;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView value;
        ImageView thumbnail;

        MyViewHolder(View view) {
            super(view);
            value = view.findViewById(R.id.value_history);
            thumbnail = view.findViewById(R.id.imagen_history);

            view.setOnClickListener(view1 ->
                    listener.onHistorySelected(resultsList.get(getAdapterPosition())));
        }
    }

    public HistoryAdapter(ArrayList<String> resultsList, HistoryAdapter.HistoryAdapterListener listener) {
        if(resultsList!=null){
            this.resultsList = resultsList;
        } else {
            this.resultsList = new ArrayList<>();
        }
        this.listener = listener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.value.setText(resultsList.get(position));
    }

    @Override
    public int getItemCount() {
        return resultsList.size();
    }

    public interface HistoryAdapterListener {
        void onHistorySelected(String resultado);
    }
}