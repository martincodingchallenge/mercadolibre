package com.martinartime.mercadolibre.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.martinartime.mercadolibre.POJO.Result;
import com.martinartime.mercadolibre.R;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Creado por MartinArtime el 27 de noviembre del 2019
 */
public class ResultAdapter extends RecyclerView.Adapter<ResultAdapter.MyViewHolder> implements Filterable {

    private Context context;
    private List<Result> resultsList;
    private List<Result> resultsListFiltered;
    private ResultAdapterListener listener;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        TextView price;
        ImageView thumbnail;

        MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.nombre_item);
            price = view.findViewById(R.id.precio_item);
            thumbnail = view.findViewById(R.id.imagen_item);

            view.setOnClickListener(view1 ->
                    listener.onResultSelected(resultsListFiltered.get(getAdapterPosition())));
        }
    }


    public ResultAdapter(Context context, List<Result> resultsList, ResultAdapterListener listener) {
        this.context = context;
        this.listener = listener;
        this.resultsList = resultsList;
        this.resultsListFiltered = resultsList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_menu, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final Result resultado = resultsListFiltered.get(position);
        holder.name.setText(resultado.getTitle());
        int price = resultado.getPrice().intValue();

        NumberFormat formatter = NumberFormat.getInstance(new Locale("da", "DK"));
        String priceToShow = formatter.format(price);
        holder.price.setText("$ "+priceToShow);

        Glide.with(context)
                .load(resultado.getThumbnail())
                .into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return resultsListFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    resultsListFiltered = resultsList;
                } else {
                    List<Result> filteredList = new ArrayList<>();
                    for (Result row : resultsList) {

                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    resultsListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = resultsListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                resultsListFiltered = (ArrayList<Result>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public interface ResultAdapterListener {
        void onResultSelected(Result resultado);
    }
}