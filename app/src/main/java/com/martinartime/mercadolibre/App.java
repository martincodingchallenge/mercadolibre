package com.martinartime.mercadolibre;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.martinartime.mercadolibre.API.APIService;
import com.martinartime.mercadolibre.API.RetrofitClient;

import static com.martinartime.mercadolibre.Utils.Util.URL_BASE;

/**
 * Creado por MartinArtime el 26 de noviembre del 2019
 */
public class App extends Application {

    public static Gson gson;
    public static APIService apiService;

    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    public static APIService getAPIService() {
        return apiService;
    }

    public static Gson getGson() {
        return gson;
    }

    @Override
    public void onCreate() {
        instance = this;
        super.onCreate();

        gson = new Gson();
        apiService = RetrofitClient.getClient(URL_BASE).create(APIService.class);
    }

}
