package com.martinartime.mercadolibre.API;

import com.martinartime.mercadolibre.POJO.APIItemDetails;
import com.martinartime.mercadolibre.POJO.APISearchResponse;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

import static com.martinartime.mercadolibre.Utils.Util.URL_ITEMS;
import static com.martinartime.mercadolibre.Utils.Util.URL_SEARCH;

/**
 * Interface that contains the signature methods with all the posisble requests to the server
 *
 *  Creado por MartinArtime el 27 de noviembre del 2019
 */
public interface APIService {

    /**
     * Query items searching signature
     */
    @GET(URL_SEARCH)
    Flowable<APISearchResponse> searchProducts(@Query("q") String query);

    /**
     * Item details searching signature
     */
    @GET(URL_ITEMS)
    Flowable<APIItemDetails> searchItem(@Query("id") String id);

}

