package com.martinartime.mercadolibre.API;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Client class for Retrofit API in order to make requests with web server
 */
public class RetrofitClient {

    private static Retrofit sRetrofit = null;

    /**
     * Get the client given a base url, we add the gson automatic converter to pojo classes and the reactive java adapter
     * @param baseUrl, URL base
     * @return retrofit object
     */
    public static Retrofit getClient(String baseUrl) {

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        if (sRetrofit==null) {
            sRetrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }

        return sRetrofit;
    }
}