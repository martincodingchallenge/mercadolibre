# MercadoLibre Coding Challenge

Esta es la resolución del coding challenge propuesto por Mercado Libre.

Decisiones que tome respecto a la arquitectura de la app:
- Intenté hacerla similar a la app real de MercadoLibre de manera que fuera fácil de identificar.
- Me pareció interesante probar encarar todo con uso de fragmentos en vez de muchas activities (que son más pesadas) y dado que en mi idea había bastante dinamismo entre vistas, el uso de fragments que se puedan intercambiar rápidamente cumplía con el requisito.
- El uso de algunos componentes externos como Shimmer son para acelerar el proceso de desarrollo y poder entregar el challenge lo antes posible.
- Use el paradigma reactivo para poder manejar las peticiones a la API de forma asincrónica y no bloquear el UI Thread.
- Utilice la 'dummy view' como herramienta visual para que el usuario sepa que está buscando lo que el quiere.
- Implementé un historial de búsquedas (como la app de MercadoLibre) para facilitar futuras búsquedas y se guarda en SharedPreferences, preferí en este caso no utilizar bases de datos u ORMs dado que la naturaleza de la app no lo requeria y llevaba a más tiempo de implementación.

Componentes externos utilizados:
- Butterknife: para bindeo de vistas.
- Retrofit: para comunicación con servidor backend.
- Gson: para (de)serialización de payloads en requests/responses a la API.
- rxJava/rxAndroid: para manejo de asincronismo en peticiones a la API.
- Glide: para manejo de fotos en la app.
- Shimmer: para simular 'dummy views' mientras se realiza la busqueda a la API.
- CarouselView: para un mejor display de las fotos de los productos en la vista de detalle.

Cosas que mejoraría en una futura versión:
- Agregar un botón abajo de la lista inicial de búsqueda para "Buscar más" (y usar el paginado de la API) o implementar un "auto fetching" donde se busquen automaticamente cuando el usuario scrollea hasta el fin de los resultados.
- Usar view binding en vez de Butterknife (que luego será deprecado).
- No depender de componentes como Shimmer o CarouselView, hacerlos propios.
- Cambiar algunas cuestiones de navegabilidad.
- Agregar más datos de los productos en detalle.
- Cambiar a Kotlin para ver cuanto simplifica la base de código.
- Ver de hacer la pantalla de detalles una activity aparte en vez de un fragment.
- Mejorar la forma de mostrar las imagenes en la pantalla de detalle.

